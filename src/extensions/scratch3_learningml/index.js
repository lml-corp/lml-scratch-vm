require("@babel/polyfill");
const ArgumentType = require('../../extension-support/argument-type');
const BlockType = require('../../extension-support/block-type');
const Runtime = require('../../engine/runtime');
const Cast = require('../../util/cast');
const base64js = require('base64-js');
const formatMessage = require('format-message');
const {
    numericalEncoder,
    bowEncoder,
    buildVocabulary,
    setVocabulary,
    getMobilenetEncoder,
    LMLModelFactory,
    transformObjectToMapWithTensors,
    combineMapsOfTensors,
    extendArraysInObject,
    collectExample,
    audioEncoder
} = require('lml-algorithms');
const getBlockIconURI = require("./blockIcon");
const Swal = require("sweetalert2");
const { serialize } = require("../../serialization/sb3");

/**
 * Icon png to be displayed at the left edge of each extension block, encoded as a data URI.
 * @type {string}
 */

const blockIconURI = getBlockIconURI();

const IMAGE_SIZE = 227;

const VideoState = {
    /** Video turned off. */
    OFF: 'off',

    /** Video turned on with default y axis mirroring. */
    ON: 'on',

    /** Video turned on without default y axis mirroring. */
    ON_FLIPPED: 'on-flipped'
};

const ModelName = {
    SEQUENTIAL: 'sequential',
    KNN: 'knn'
}

const SequentialHyperparam = {
    EPOCHS: 'epochs',
    BATCH_SIZE: 'batchSize',
    LEARNING_RATE: 'learningRate'
}

const KNNHyperparam = {
    K: 'number of neighbors'
}

const InputType = {
    VIDEO: 'video',
    COSTUME: 'costume',
    TEXT: 'text',
    NUMBERS: 'numbers'
}

const ModelType = {
    TEXT: 'text',
    IMAGE: 'image',
    NUMBER: 'numerical'
}

class Scratch3LearningMLBlocks {
    /**
     * @return {string} - the name of this extension.
     */
    static get EXTENSION_NAME() {
        return 'learningml';
    }

    /**
     * @return {string} - the ID of this extension.
     */
    static get EXTENSION_ID() {
        return 'learningml';
    }

    /**
     * Construct a set of Echidna blocks.
     * @param {Runtime} runtime - the Scratch 3.0 runtime.
     */
    constructor(runtime) {

        this.runtime = runtime;        

        this.runtime.on(Runtime.PROJECT_LOADED, () => {
            console.log("PROJECT_LOADED event fired");
            // this.runtime.modelZipContent es definido solo cuando se ha realizado
            // la carga de un modelo desde el menú `Archivo -> Load ML Model from your computer`
            this.runtime.modelZipContent = null;
            this.loadModel();
            console.log("PROJECT LOADED");
        });

        this.bcEditor = new BroadcastChannel('lml-editor');
        this.bcEditor.addEventListener('message', message => {
            if (message.data == 'updateModel') {
                this.resetDataset();
                this.resetFeatures();
                this.loadModel();
            }

            if (message.data == 'showModelInfo') {
                this.showModelInfo();
            }
        })

    }

    showModelInfo() {
        Swal.fire({
            title: "Información sobre el Modelo",
            html: `                    
                <p>
                    <b>Nombre</b>: ${this.runtime.lmlModelMetadata.data.name}
                </p>
                ${('dimension' in this.runtime.lmlModelMetadata.data && this.runtime.lmlModelMetadata.data.type == 'numerical') ?
                    '<p><b>Dimensión:</b> ' + this.runtime.lmlModelMetadata.data.dimension + '</p>' :
                    ''
                }
                <p>
                    <b>Tipo de dato:</b> ${this.runtime.lmlModelMetadata.data.type}
                </p> 
                <p>
                    <b>Algoritmo:</b> ${this.runtime.lmlModelMetadata.model.metadata.modelAlgorithm}
                </p>
                <p>
                    <b>Etiquetas:</b> ${this.runtime.lmlModelMetadata.model.metadata.labels.join(",")}
                </p> 
                <p>
                    <b>Hiperparámetros:</b> ${JSON.stringify(this.runtime.lmlModelMetadata.model.metadata.hyperparameters)}
                </p>                 
          `,
        });
    }    

    loadModel() {
        console.log("LOAD MODEL");
        let that = this;
        return LMLModelFactory.load(this.runtime.modelZipContent).then(result => {
            that.runtime.lmlModelMetadata = result.lmlModelMetadata;
            that.runtime.lmlModel = result.lmlModel;
            this.setEncoder(that.runtime.lmlModelMetadata.data.type);
            if (result.loadedFrom == 'disk') {
                this.showModelInfo();
            }
            return result;
        });
    }

    resetDataset() {
        this.dataset = new Map();
        try {
            this.runtime.lmlModelMetadata.model.metadata.labels = [];
        } catch {
            console.log("this.runtime.lmlModelMetadata.model.metadata.label aún no definido");
        }
    }

    resetFeatures() {
        if (this.featureset) {
            this.featureset.forEach((element, key) => {
                this.featureset.get(key).dispose();
            });
        }

        this.featureset = new Map();
    }

    setModelDataType(modelDataType) {
        this.resetDataset();
        this.resetFeatures();
        this.setEncoder(modelDataType);
    }

    setModelAlgorithm(modelAlgorithm) {
        this.runtime.lmlModelMetadata.model.metadata.modelAlgorithm = modelAlgorithm;
        this.runtime.lmlModel = LMLModelFactory.createModel(modelAlgorithm);
    }

    setEncoder(modelDataType) {
        this.runtime.lmlModelMetadata.data.type = modelDataType;
        switch (modelDataType) {
            case 'text':
                this.encoder = bowEncoder;
                break;
            case 'image':
                this.encoder = getMobilenetEncoder();
                break;
            case 'numerical':
                this.encoder = numericalEncoder;
                break;
            case 'audio':
                this.encoder = audioEncoder;
        }
    }

    setModelHyperParameters() {
        this.runtime.lmlModel.setHyperParameters(this.runtime.lmlModelMetadata.model.metadata.hyperparameters);
    }

    saveModelToLocalstorage() {
        let datatype = this.runtime.lmlModelMetadata.data;
        let encoder = this.runtime.lmlModelMetadata.encoder;
        return this.runtime.lmlModel.saveToLocalstorage(datatype, encoder).then(r => {
            return r;
        });
    }

    encodeDataURI(asset) {
        let data = base64js.fromByteArray(asset.data)
        return `data:${asset.assetType.contentType};base64,${data}`;
    }

    getBlocks() {
        let blocks = [
            {
                opcode: 'debug',
                blockType: BlockType.REPORTER,
                text: formatMessage({
                    id: 'learningml.debug',
                    default: 'DEBUG',
                    description: 'DEBUG'
                }),
                arguments: {
                    ITEM: {
                        type: ArgumentType.STRING,
                        defaultValue: 'item'
                    }
                }
            },
            {
                opcode: 'classifyItem',
                blockType: BlockType.REPORTER,
                text: formatMessage({
                    id: 'learningml.classifyItem',
                    default: 'Classify item [ITEM]',
                    description: 'classify an item'
                }),
                arguments: {
                    ITEM: {
                        type: ArgumentType.STRING,
                        defaultValue: 'item'
                    }
                }
            },

            {
                opcode: 'confidenceItem',
                blockType: BlockType.REPORTER,
                text: formatMessage({
                    id: 'learningml.confidenceItem',
                    default: 'Confidence for item [ITEM]',
                    description: 'Confidence of a classificationm'
                }),
                arguments: {
                    ITEM: {
                        type: ArgumentType.STRING,
                        defaultValue: 'item'
                    }
                }
            },
            {
                opcode: 'currentCostume',
                blockType: BlockType.REPORTER,
                text: formatMessage({
                    id: 'learningml.currentcostume',
                    default: 'Current costume',
                    description: 'Current costume'
                }),
            },
            {
                opcode: 'videoImage',
                blockType: BlockType.REPORTER,
                text: formatMessage({
                    id: 'learningml.videoimage',
                    default: 'Video image',
                    description: 'Video image'
                }),
            },
            {
                opcode: 'videoToggle',
                text: formatMessage({
                    id: 'learningml.turnvideoonoff',
                    default: 'Turn video [VIDEO_STATE]',
                    description: 'Turn video On/Off'
                }),
                arguments: {
                    VIDEO_STATE: {
                        type: ArgumentType.NUMBER,
                        menu: 'VIDEO_STATE',
                        defaultValue: VideoState.ON
                    }
                }
            },
            {
                opcode: 'recordAudio',
                blockType: BlockType.REPORTER,
                text: formatMessage({
                    id: 'learningml.recordaudio',
                    default: 'Record audio',
                    description: 'Record audio'
                }),
            },
        ];

        blocks = blocks.concat(
            [
                {
                    opcode: 'opsetModelDataType',
                    text: formatMessage({
                        id: 'learningml.opsetModelDataType',
                        default: 'Set model type to [MODEL_TYPE]',
                        description: 'Set model type'
                    }),
                    arguments: {
                        MODEL_TYPE: {
                            type: ArgumentType.STRING,
                            menu: 'MODEL_TYPE',
                            defaultValue: ModelType.TEXT
                        },
                        VALUE: {
                            type: ArgumentType.STRING
                        }
                    }
                },
                {
                    opcode: 'setDimension',
                    text: formatMessage({
                        id: 'learningml.setDimension',
                        default: 'Set dimension to [DIMENSION]',
                        description: 'Set dimension to set of numbers recognition'
                    }),
                    arguments: {
                        DIMENSION: {
                            type: ArgumentType.NUMBER,
                            defaultValue: 4
                        }
                    }
                },
                {
                    opcode: 'opsetModelAlgorithm',
                    text: formatMessage({
                        id: 'learningml.opsetModelAlgorithm',
                        default: 'Use [MODEL_ALGORITHM] for learning',
                        description: 'Choose Machine Learning ModelType to build a model'
                    }),
                    arguments: {
                        MODEL_ALGORITHM: {
                            type: ArgumentType.STRING,
                            menu: 'MODEL_ALGORITHM_INFO',
                            defaultValue: ModelName.SEQUENTIAL
                        }
                    }
                },
                {
                    opcode: 'setSequentialHyperParam',
                    text: formatMessage({
                        id: 'learningml.setSequentialHyperParam',
                        default: 'Set [SEQUENTIAL_HYPERPARAM] to [VALUE]',
                        description: 'Set epochs hyperparam'
                    }),
                    arguments: {
                        SEQUENTIAL_HYPERPARAM: {
                            type: ArgumentType.STRING,
                            menu: 'SEQUENTIAL_HYPERPARAM',
                            defaultValue: SequentialHyperparam.EPOCHS
                        },
                        VALUE: {
                            type: ArgumentType.NUMBER
                        }
                    }
                },
                {
                    opcode: 'setKNNHyperParam',
                    text: formatMessage({
                        id: 'learningml.setKNNHyperParam',
                        default: 'Set [KNN_HYPERPARAM] to [VALUE]',
                        description: 'Set epochs hyperparam'
                    }),
                    arguments: {
                        KNN_HYPERPARAM: {
                            type: ArgumentType.STRING,
                            menu: 'KNN_HYPERPARAM',
                            defaultValue: KNNHyperparam.SEQUENTIAL
                        },
                        VALUE: {
                            type: ArgumentType.NUMBER
                        }
                    }
                },
                {
                    opcode: 'addItemToLabel',
                    blockType: BlockType.COMMAND,
                    text: formatMessage({
                        id: 'learningml.addItemToLabel',
                        default: 'Add item [ITEM] to label [LABEL]',
                        description: 'Add an item to a label or class'
                    }),
                    arguments: {
                        ITEM: {
                            type: ArgumentType.STRING,
                            defaultValue: 'item'
                        },
                        LABEL: {
                            type: ArgumentType.STRING,
                            defaultValue: 'label'
                        }
                    }
                },
                {
                    opcode: 'removeLabel',
                    blockType: BlockType.COMMAND,
                    text: formatMessage({
                        id: 'learningml.removeLabel',
                        default: 'Remove label [LABEL]',
                        description: 'Remove a label or class'
                    }),
                    arguments: {
                        ITEM: {
                            type: ArgumentType.STRING,
                            defaultValue: 'item'
                        },
                        LABEL: {
                            type: ArgumentType.STRING,
                            defaultValue: 'label'
                        }
                    }
                },
                {
                    opcode: 'learnAndWait',
                    text: formatMessage({
                        id: 'learningml.learnAndWait',
                        default: 'Learn and wait',
                        description: 'Build a new model'
                    }),
                },
            ]
        )


        console.log(blocks);
        return blocks;
    }

    getInfo() {

        return {
            id: Scratch3LearningMLBlocks.EXTENSION_ID,
            name: Scratch3LearningMLBlocks.EXTENSION_NAME,
            blockIconURI: blockIconURI,
            showStatusButton: false,
            color1: "#61CE70",
            color2: "#619E70",
            color3: "#3d3a37",
            blocks: this.getBlocks(),
            menus: {

                VIDEO_STATE: {
                    acceptReporters: true,
                    items: this.VIDEO_STATE_INFO
                },
                MODEL_ALGORITHM_INFO: {
                    acceptReporters: true,
                    items: this.MODEL_ALGORITHM_INFO
                },
                SEQUENTIAL_HYPERPARAM: {
                    acceptReporters: true,
                    items: this.SEQUENTIAL_HYPERPARAM_INFO
                },
                KNN_HYPERPARAM: {
                    acceptReporters: true,
                    items: this.KNN_HYPERPARAM_INFO
                },
                MODEL_TYPE: {
                    acceptReporters: true,
                    items: this.MODEL_TYPE_INFO
                }

            }
        };
    }
    classify(item) {
        console.log(item);
        console.log("CLASSIFY");
        if (!this.encoder) {
            return 'NO ENCODER YET';
        }

        if (!this.runtime.lmlModel.model) {
            return 'NO MODEL YET';
        }

        if (this.runtime.lmlModelMetadata.data.type == 'text') {
            setVocabulary(this.runtime.lmlModelMetadata.encoder.vocabulary);
        }

        if (this.runtime.lmlModelMetadata.data.type == 'audio') {
            item = this.deserializeSound(item);
        }

        return this.encoder([item]).then(features => {
            return this.runtime.lmlModel.classify(features);
        }).then(results => {
            console.log(results);
            return results[0][0]
        })
    }

    confidence(item) {
        if (!this.encoder) {
            return 'NO ENCODER YET';
        }

        if (!this.runtime.lmlModel.model) {
            return 'NO MODEL YET';
        }

        if (this.runtime.lmlModelMetadata.data.type == 'audio') {
            item = this.deserializeSound(item);
        }

        return this.encoder([item]).then(features => {
            return this.runtime.lmlModel.classify(features, this.runtime.lmlModelMetadata.model.metadata.labels);
        }).then(results => {
            console.log(results);
            let r = parseFloat(100 * results[0][1]).toFixed(3).toString();
            if (r.includes('.')) {
                r = r.replace(/\.?0+$/, '');
            }
            return r;
        })

    }

    classifyItem(args, utils) {
        if (this.runtime.lmlModelMetadata.data.type == 'image') {
            if (args.ITEM == '%__video__%') {
                return this._classifyVideoImage();
            } else {
                return this._classifyCostumeImage(args, utils);
            }
        } else {
            return this.classify(args.ITEM);
        }

    }

    serializeSound(obj) {
        return JSON.stringify({
          ...obj,
          rawAudio: {
            ...obj.rawAudio,
            data: Array.from(obj.rawAudio.data), // Convertir Float32Array a Array
          },
          spectrogram: {
            ...obj.spectrogram,
            data: Array.from(obj.spectrogram.data), // Convertir Float32Array a Array
          },
        });
      }
      
      // Deserializar el objeto
      deserializeSound(serializedObj) {
        const obj = JSON.parse(serializedObj);
        return {
          ...obj,
          rawAudio: {
            ...obj.rawAudio,
            data: new Float32Array(obj.rawAudio.data), // Reconstruir Float32Array
          },
          spectrogram: {
            ...obj.spectrogram,
            data: new Float32Array(obj.spectrogram.data), // Reconstruir Float32Array
          },
        };
      }

    confidenceItem(args, utils) {
        if (this.runtime.lmlModelMetadata.data.type == 'image') {
            if (args.ITEM == '%__video__%') {
                return this._classifyVideoImage(false);
            } else {
                return this._classifyCostumeImage(args, utils, false);
            }
        } else {
            return this.confidence(args.ITEM);
        }
    }

    get VIDEO_STATE_INFO() {
        return [{
            text: 'OFF',
            value: VideoState.OFF
        },
        {
            text: 'ON',
            value: VideoState.ON
        },
        {
            text: 'ON FLIPPED',
            value: VideoState.ON_FLIPPED
        }
        ];
    }

    _classifyCostumeImage(args, utils, classify = true) {
        let sprite = utils.target.sprite.costumes[args.ITEM - 1];

        if (sprite == undefined)
            return Promise.resolve("NO_IMAGE");

        let src = this.encodeDataURI(sprite.asset);

        if (classify) {
            return this.classify(src);
        } else {
            return this.confidence(src);
        }


    }

    _classifyVideoImage(classify = true) {

        if (this.runtime.ioDevices.video.provider._video != null) {

            // Get image data from video element
            let video = this.runtime.ioDevices.video.provider._video;
            video.height = IMAGE_SIZE;
            video.width = IMAGE_SIZE;

            let src = this.takeSnapshot(video);

            if (classify) {
                return this.classify(src);
            } else {
                return this.confidence(src);
            }

        }

        return Promise.resolve("NO VIDEO");
    }


    currentCostume(args, utils) {
        return utils.target.currentCostume + 1;
    }

    videoImage(args, utils) {
        return '%__video__%';
    }

    videoToggle(args) {
        const state = args.VIDEO_STATE;
        this.globalVideoState = state;
        if (state === VideoState.OFF) {
            this.runtime.ioDevices.video.disableVideo();
        } else {
            this.runtime.ioDevices.video.enableVideo();
            // Mirror if state is ON. Do not mirror if state is ON_FLIPPED.
            this.runtime.ioDevices.video.mirror = state === VideoState.ON;
        }
    }

    takeSnapshot(video) {
        let draw = document.createElement("canvas");
        draw.width = video.videoWidth;
        draw.height = video.videoHeight;
        let context2D = draw.getContext("2d");
        context2D.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);

        let b64Img = draw.toDataURL('image/jpeg', 0.5);

        return b64Img;
    }

    recordAudio(args) {
        console.log(args);
        return collectExample("__").then(result => {
            return this.serializeSound({
                'rawAudio': result.rawAudio,
                'spectrogram': result.spectrogram
            })
        })
    }

    addItemToLabel(args, utils) {
        let item;
        let label = args.LABEL;

        switch (this.runtime.lmlModelMetadata.data.type) {
            case 'text':
                if (typeof args.ITEM != 'string' || args.ITEM == '%__video__%') {
                    return Promise.resolve('BAD STRING');
                }
                item = args.ITEM;
                break;
            case 'image':
                if (args.ITEM == '%__video__%') {
                    let video = this.runtime.ioDevices.video.provider._video;
                    if (!video) return Promise.resolve("video is off");

                    video.height = IMAGE_SIZE;
                    video.width = IMAGE_SIZE;

                    item = this.takeSnapshot(video);

                } else if (Number.isInteger(args.ITEM)) {
                    let sprite = utils.target.sprite.costumes[args.ITEM - 1];
                    item = this.encodeDataURI(sprite.asset);
                } else {
                    return Promise.resolve('BAD IMAGE');
                }
                break;
            case 'numerical':
                if (!this.isValidNumberEntry(args.ITEM)) {
                    return Promise.resolve('BAD NUMBER OR BAD DIMENSION');
                }
                item = args.ITEM;
                break;
            case 'audio':
                item = this.deserializeSound(args.ITEM);                

        }

        if (!('labels' in this.runtime.lmlModelMetadata.model.metadata)) {
            this.runtime.lmlModelMetadata.model.metadata.labels = [];
        }

        if (!this.dataset) {
            this.dataset = new Map();
        }

        if (!this.dataset.has(label)) {
            /**
             * Podría ocurrir que se esté construyendo un nuevo dataset existiendo ya
             * un modelo en el localStorage y, por tanto, existiendo la etiqueta en
             * this.runtime.lmlModelMetadata.model.metadata.labels. En ese caso no hay 
             * que añadir la etiqueta a this.runtime.lmlModelMetadata.model.metadata.labels
             */
            if (!(this.runtime.lmlModelMetadata.model.metadata.labels.includes(label))) {
                this.runtime.lmlModelMetadata.model.metadata.labels.push(label);
            }
            this.dataset.set(label, new Set());
        }

        this.dataset.set(label, this.dataset.get(label).add(item));

        console.log(this.dataset);

        return Promise.resolve("OK");

    }

    removeLabel(args) {
        this.dataset.delete(args.LABEL);
        this.featureset.delete(args.LABEL);
        const index = this.runtime.lmlModelMetadata.model.metadata.labels.indexOf(args.LABEL);
        if (index > -1) {
            this.runtime.lmlModelMetadata.model.metadata.labels.splice(index, 1);
        }

        console.log(this.dataset);
        console.log(this.featureset);
    }


    fromCSV2Array(csv) {
        return csv.split(",").map(v => parseFloat(v));
    }

    isValidNumberEntry(entry) {
        let items = this.fromCSV2Array(entry);

        // primero miramos que todas los items separados por coma sean números
        if (items.some(item => isNaN(item))) return false;

        // después comprobamos si this.runtime.lmlModelMetadata.data.dimension es nulo, lo cual significa que es
        // la primera entrada y será la que define la dimensión de los vectores
        if (this.runtime.lmlModelMetadata.data.dimension == undefined) {
            this.runtime.lmlModelMetadata.data.dimension = items.length;
        }

        // por último comprobamos que la dimensión del array items coincida con this.runtime.lmlModelMetadata.data.dimension
        if (this.runtime.lmlModelMetadata.data.dimension != items.length) return false;

        // si hemos llegado hasta aquí, todo está bien
        return true;

    }

    opsetModelDataType(args) {
        this.setModelDataType(args.MODEL_TYPE);
    }

    opsetModelAlgorithm(args) {
        this.setModelAlgorithm(args.MODEL_ALGORITHM);
    }

    setSequentialHyperParam(args) {
        console.log(args);
        switch (args.SEQUENTIAL_HYPERPARAM) {
            case 'epochs':
                this.runtime.lmlModelMetadata.model.metadata.hyperparameters.epochs = parseInt(args.VALUE);
                break;
            case 'batchSize':
                this.runtime.lmlModelMetadata.model.metadata.hyperparameters.batchSize = parseInt(args.VALUE);
                break;
            case 'learningRate':
                this.runtime.lmlModelMetadata.model.metadata.hyperparameters.learningRate = parseFloat(args.VALUE);
                break;

        }

    }

    setKNNHyperParam(args) {
        console.log(args);
        this.runtime.lmlModelMetadata.model.metadata.hyperparameters.K = parseInt(args.VALUE);
    }

    setDimension(args) {
        this.runtime.lmlModelMetadata.data.dimension = args.DIMENSION;
    }


    learnAndWait(args) {

        /**
         * En el mismo momento en que se genera un nuevo modelo, este ya no coincide con
         * el modelo que se generó en LML-Editor quedando, por tanto, desincronizado.
         * Así que el objeto `lmlModel` del localStorage ya no es necesario, pues solo se usó
         * para generar el modelo cuando se venía de LML-Editor o se cargó el modelo desde disco.
         * Para evitar confusiones, en este momento se borra el `lmlModel` del localStorage.
         * 
         * Si se vuelve a generar un modelo con LML-Editor o cargando desde disco, `lmlModel`
         * volverá a crearse en el localStorage y un nuevo modelo se cargará.
         */
        localStorage.removeItem('lmlModel');
        this.setModelHyperParameters();
        this.resetFeatures();

        let promises = [];

        /**
         * Si el modelo es de tipo texto, al ser el encoder dependiente
         * del vocabulario y este, a su vez, dependiente de los textos de
         * ejemplo, hay que construir o reconstruir el vocabulario
         */
        if (this.runtime.lmlModelMetadata.data.type == 'text') {
            let texts = Array.from(this.dataset.values()).map(set => Array.from(set)).flat();
            let vocabulary = buildVocabulary(texts);

            /**
             * si ya se ha cargado un modelo desde el localstorage o desde disco
             * hay que ampliar el vocabulario y extender la dimensión de las features
             * a la longitud del nuevo vocabulario
             */
            if ('encoder' in this.runtime.lmlModelMetadata) {
                let voc0 = this.runtime.lmlModelMetadata.encoder.vocabulary
                let len0 = voc0.length;
                this.runtime.lmlModelMetadata.encoder.vocabulary =
                    voc0.concat(vocabulary.filter(item => !voc0.includes(item)));
                let len1 = this.runtime.lmlModelMetadata.encoder.vocabulary.length;
                setVocabulary(this.runtime.lmlModelMetadata.encoder.vocabulary);

                this.runtime.lmlModelMetadata.model.features =
                    extendArraysInObject(this.runtime.lmlModelMetadata.model.features, len1 - len0);
            }
            /**
             * si el modelo se está creando desde cero (no viene del localstorage
             * o del disco), hay que definir this.runtime.lmlModelMetadata.encoder.vocabulary
             */
            else {
                this.runtime.lmlModelMetadata.encoder = { vocabulary };
            }

        }

        /**
         * Aquí se crea el featureset a partir de los datos que se hayan añadido a 
         * través del bloque add item to label
         */
        this.dataset.forEach((element, key) => {
            promises.push(this.encoder(Array.from(element)).then(features => {
                this.featureset.set(key, features);
            }));
        });

        return Promise.all(promises).then(() => {
            /**
             * si ya se ha cargado un modelo desde el localstorage o desde disco, hay que 
             * ampliar las features originales con las que se hayan añadido con el bloque
             * add item to label
             */
            if ('features' in this.runtime.lmlModelMetadata.model) {
                let originalFeatures = transformObjectToMapWithTensors(this.runtime.lmlModelMetadata.model.features);
                this.featureset = combineMapsOfTensors(this.featureset, originalFeatures);
            }
            console.log(this.featureset);
            return this.runtime.lmlModel.train(this.featureset).then(result => {
                console.log(result);
                /**
                 * Como consecuencia de haber fusionado los datos que ya estaban en el localStorage
                 * con los que se han añadido a través de addItemToLabel, el número de etiquetas y/o
                 * el orden de estas ha podido variar, por esto hay que actualizar las que estan en el 
                 * runtime con las que el proceso de entrenamiento ha definido.
                 */
                this.runtime.lmlModelMetadata.model.metadata.labels = this.runtime.lmlModel.labels;

                return result;
            });
        });
    }

    debug() {
        console.log(this.dataset);
        console.log(this.featureset);
        console.log(this.runtime.lmlModel);
        console.log("this.runtime.lmlModelMetadata");
        console.log(this.runtime.lmlModelMetadata);
        console.log("END DEBUG");

    }

    get MODEL_ALGORITHM_INFO() {
        return [{
            text: 'sequential',
            value: ModelName.SEQUENTIAL
        },
        {
            text: 'knn',
            value: ModelName.KNN
        }
        ];
    }

    get SEQUENTIAL_HYPERPARAM_INFO() {
        return [
            {
                text: 'Epochs',
                value: SequentialHyperparam.EPOCHS
            },
            {
                text: 'Batch size',
                value: SequentialHyperparam.BATCH_SIZE
            },
            {
                text: 'Learning rate',
                value: SequentialHyperparam.LEARNING_RATE
            }
        ]
    }

    get KNN_HYPERPARAM_INFO() {
        return [
            {
                text: 'Number of neighbors',
                value: KNNHyperparam.K
            }
        ]
    }

    get MODEL_TYPE_INFO() {
        return [
            {
                text: 'Text',
                value: 'text'
            },
            {
                text: 'Image',
                value: 'image'
            },
            {
                text: 'Numerical',
                value: 'numerical'
            },
            {
                text: 'Audio',
                value: 'audio'
            },
        ];
    }

}

module.exports = Scratch3LearningMLBlocks;