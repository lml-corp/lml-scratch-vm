# lml-scratch-vm

ATENCIÓN: Se puede compilar con node 20.11.0

## Kakakakaka

Para activar el modo avanzado:

```
http://la.url?advanced=1
```

El encoder `mobilenet` requiere descargar los parámetros del modelo de alguna url. Por defecto la descarga de `http://localhost:5173`, que es donde se sirve la aplicación `lml-editor` y que, obviamente, sirve dichos parámetros que necesita `mobilenet`. Cuando se coloque en un servidor con un dominio distinto, esa URL se puede pasar a lml-scratch así:

```
http://la.url?url_models=http://la.url.de.los.parametros
```

El archivo donde se gestiona la descarga en local del proyecto scratch: `src/virtual-machine.js`, función `saveProjectSb3`.

He tocado la función `serialize` de `src/serialization/sb3.js` para añadir el dataset que, a su vez, se ha añadido en el runtime. De esta manera se guarda dicho dataset en el fichero sb3.


## Repository organization

- Branch `develop` has been cloned from https://github.com/scratchfoundation/scratch-vm.git, and has been uploaded to this repo in order to incorporate new code in the future. This branch match with tag tag: v2.1.11.

- Branch `dev` has been created from the `develop` branch (8bb56845489cb651b4bce04874e3be74410b9619). Branch `dev` will be our develop branch where changes for LML project will be made. Once these changes have been tested enough, they will be merged with main.

In order to merge changes made by the Scratch team in our `dev` branch, you must follow the following steps:

    # git clone https://gitlab.com/lml-corp/lml-scratch-vm
    # git remote add upstream https://github.com/scratchfoundation/scratch-vm
    # git fetch upstream develop
    # git checkout develop
    # git merge upstream/develop
    # git checkout dev
    # git merge develop

Conflicts that appear must be resolved before continuing to develop new features or mergin with `main`.

# Original README.md

## scratch-vm
#### Scratch VM is a library for representing, running, and maintaining the state of computer programs written using [Scratch Blocks](https://github.com/scratchfoundation/scratch-blocks).

[![CI/CD](https://github.com/scratchfoundation/scratch-vm/actions/workflows/ci-cd.yml/badge.svg)](https://github.com/scratchfoundation/scratch-vm/actions/workflows/ci-cd.yml)

## Installation
This requires you to have Git and Node.js installed.

To install as a dependency for your own application:
```bash
npm install scratch-vm
```
To set up a development environment to edit scratch-vm yourself:
```bash
git clone https://github.com/scratchfoundation/scratch-vm.git
cd scratch-vm
npm install
```

## Development Server
This requires Node.js to be installed.

For convenience, we've included a development server with the VM. This is sometimes useful when running in an environment that's loading remote resources (e.g., SVGs from the Scratch server). If you would like to use your modified VM with the full Scratch 3.0 GUI, [follow the instructions to link the VM to the GUI](https://github.com/scratchfoundation/scratch-gui/wiki/Getting-Started).

## Running the Development Server
Open a Command Prompt or Terminal in the repository and run:
```bash
npm start
```

## Playground
To view the Playground, make sure the dev server's running and go to [http://localhost:8073/playground/](http://localhost:8073/playground/) - you will be directed to the playground, which demonstrates various tools and internal state.

![VM Playground Screenshot](https://i.imgur.com/nOCNqEc.gif)


## Standalone Build
```bash
npm run build
```

```html
<script src="/path/to/dist/web/scratch-vm.js"></script>
<script>
    var vm = new window.VirtualMachine();
    // do things
</script>
```

## How to include in a Node.js App
For an extended setup example, check out the /src/playground directory, which includes a fully running VM instance.
```js
var VirtualMachine = require('scratch-vm');
var vm = new VirtualMachine();

// Block events
Scratch.workspace.addChangeListener(vm.blockListener);

// Run threads
vm.start();
```

## Abstract Syntax Tree

#### Overview
The Virtual Machine constructs and maintains the state of an [Abstract Syntax Tree](https://en.wikipedia.org/wiki/Abstract_syntax_tree) (AST) by listening to events emitted by the [scratch-blocks](https://github.com/scratchfoundation/scratch-blocks) workspace via the `blockListener`. Each target (code-running object, for example, a sprite) keeps an AST for its blocks. At any time, the current state of an AST can be viewed by inspecting the `vm.runtime.targets[...].blocks` object.

#### Anatomy of a Block
The VM's block representation contains all the important information for execution and storage. Here's an example representing the "when key pressed" script on a workspace:
```json
{
  "_blocks": {
    "Q]PK~yJ@BTV8Y~FfISeo": {
      "id": "Q]PK~yJ@BTV8Y~FfISeo",
      "opcode": "event_whenkeypressed",
      "inputs": {
      },
      "fields": {
        "KEY_OPTION": {
          "name": "KEY_OPTION",
          "value": "space"
        }
      },
      "next": null,
      "topLevel": true,
      "parent": null,
      "shadow": false,
      "x": -69.333333333333,
      "y": 174
    }
  },
  "_scripts": [
    "Q]PK~yJ@BTV8Y~FfISeo"
  ]
}
```

## Testing
```bash
npm test
```

```bash
npm run coverage
```

## Publishing to GitHub Pages
```bash
npm run deploy
```

This will push the currently built playground to the gh-pages branch of the
currently tracked remote.  If you would like to change where to push to, add
a repo url argument:
```bash
npm run deploy -- -r <your repo url>
```

## Donate
We provide [Scratch](https://scratch.mit.edu) free of charge, and want to keep it that way! Please consider making a [donation](https://secure.donationpay.org/scratchfoundation/) to support our continued engineering, design, community, and resource development efforts. Donations of any size are appreciated. Thank you!
